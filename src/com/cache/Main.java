package com.cache;

public class Main {

	public static void main(String[] args) {
		
		Singleton s = Singleton.getInstance();
		
		s.setName("singleton");
		
		Singleton s2 = Singleton.getInstance();
		
		System.out.println(s2.getName());
		
		intPrinter p1= new intPrinter();
		intPrinter p2= new intPrinter();
		intPrinter p3= new intPrinter();
		intPrinter p4= new intPrinter();
		intPrinter p5= new intPrinter();
		Thread t1 = new Thread(p1);
		Thread t2 = new Thread(p2);
		Thread t3 = new Thread(p3);
		Thread t4 = new Thread(p4);
		Thread t5 = new Thread(p5);
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();

	}

}
