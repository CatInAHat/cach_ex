package com.cache;

public class Singleton {
	
	private Singleton(){}
	
	private static Singleton instance;
	
	public static Singleton getInstance(){
		if(instance==null)
			instance = new Singleton();
		return instance;		
	}
		
	public void DoWork(){}
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
