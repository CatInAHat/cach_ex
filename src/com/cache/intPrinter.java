package com.cache;

import java.util.Random;

public class intPrinter implements Runnable{

	public boolean isRunning = true;
	private static Object token = new Object();
	
	@Override
	public synchronized void run() {
		
		System.out.println("thread: " + Thread.currentThread().getId());
		synchronized(token)
		{
			try{
				for(int i= 1;i<10;i++){
					Random r = new Random();
					Thread.sleep((long)(3000*r.nextDouble()));
					System.out.println(i);
				}}
			catch(Exception ex){
				ex.printStackTrace();
		}
			isRunning=false;
			System.out.println("koniec.");
		}
	}

		
}
